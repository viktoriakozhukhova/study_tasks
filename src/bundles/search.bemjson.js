module.exports = {
  block: 'page',
  content: [
    {
      block: 'search', content: [
        {elem: 'input', type: 'search', placeholder: 'Search text'},
        {elem: 'icon', cls: 'fa fa-search'},
      ],
    },
  ],
};


