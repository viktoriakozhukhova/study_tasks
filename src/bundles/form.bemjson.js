module.exports = {
    block: 'page',
    title: 'Пустая',
    content: [
        {block: 'wrapper',
        content: [
            {block: 'form',
            content: [
              {block: 'h', size: 3, content: 'Авторизация'},
                {cls: 'form-group', content: [
                        {tag: 'label', content: 'Email'}, {tag: 'input', cls: 'form-control', type: 'text'},
                    ],
                },
                {cls: 'form-group', content: [
                        {tag: 'label', content: 'Password'}, {tag: 'input', cls: 'form-control', type: 'password'},
                    ],
                },
                {block: 'btn', content: 'Log In'},
            ],
            },
        ],
        },
    ],
};


