module.exports = {
  block: 'page',
  content: [
    {
      block: 'control-wrapper',
      content: [
        {elem: 'input', id: 'control'},
        {elem: 'label', for: 'control', content: 'Ваше имя'},
      ],
    },
  ],
};


