module.exports = {
  block: 'page',
  content: [
    {
      block: 'table',
      content: [
        {elem: 'thead', content: [
            {elem: 'tr', content: [
                {elem: 'th', content: '#'},
                {elem: 'th', content: 'Имя'},
                {elem: 'th', content: 'Фамилия'},
                {elem: 'th', content: 'Username'},
              ],
            },
          ],
        },
        {elem: 'tbody', content: [...new Array(30)].map((v, index) => ({
            elem: 'tr', content: [
                {elem: 'td', content: index+1},
                {elem: 'td', content: 'Mark'},
                {elem: 'td', content: 'Oto'},
                {elem: 'td', content: '@name'},
              ],
          }))},
          ],
        },
      ],
};
