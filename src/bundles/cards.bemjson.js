module.exports = {
  block: 'page',
  content: [
    {cls: 'container', content: [
        {block: 'my-row', content: [...new Array(10)].map((v, index) => ({
            elem: 'col', content: [
              {block: 'card', content: [
                  {block: 'img', mods: {lazy: true}, src: `https://via.placeholder.com/${Math.ceil(Math.random() * 10) + 300}x${Math.ceil(Math.random() * 100) + 100}`},
                  {elem: 'caption', content: [
                      {block: 'h', size: 3, content: 'Название'},
                      {elem: 'description', content: index % 2 ? [
                            {tag: 'p', content: 'Дргой текст'},
                            {tag: 'p', content: 'Другй текст'},
                          ]:
                          [
                            {tag: 'p', content: 'Другой текст'},
                            {tag: 'p', content: 'Другой текст'},
                            {tag: 'p', content: 'Другой текст'},
                            {tag: 'p', content: 'Другой текст'},
                          ]},
                      {block: 'btn', content: 'Купить'},
                    ],
                  },
                ],
              },
            ],
          }))},
        {block: 'row', content: [...new Array(10)].map((v, index) => ({
            cls: 'col-12 col-sm-6 col-lg-4', content: [
              {block: 'card', content: [
                  {block: 'img', mods: {lazy: true}, src: `https://via.placeholder.com/${Math.ceil(Math.random() * 200) + 300}x${Math.ceil(Math.random() * 100) + 100}`},
                  {elem: 'caption', content: [
                      {block: 'h', size: 3, content: 'Название'},
                      {elem: 'description', content: index % 2 ? [
                            {tag: 'p', content: 'Дргой текст'},
                            {tag: 'p', content: 'Другй текст'},
                          ]:
                          [
                            {tag: 'p', content: 'Другой текст'},
                            {tag: 'p', content: 'Другой текст'},
                            {tag: 'p', content: 'Другой текст'},
                            {tag: 'p', content: 'Другой текст'},
                          ]},
                      {block: 'btn', content: 'Купить'},
                    ],
                  },
                ],
              },
            ],
          }))},
      ],
    },
  ],
};
