module.exports = {
  block: 'page',
  title: 'сетка',
  content: [
    {block: 'layout-wrapper', content: [
        {block: 'layout-header', content: [
            {tag: 'p', content: 'Header text'},
          ],
        },
        {block: 'layout-wrapper2',
          content: [
            {elem: 'side1', content: 'a semper ante tincidunt sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat. Quisque ultrices velit vitae bibendum lobortis. Fusce posuere malesuada libero eget finibus. Mauris cursus lacinia auctor. Aliquam efficitur metus ac erat efficitur semper. Aenean aliquam nulla ac ex consectetur convallis.'},
            {elem: 'side2', content: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat.'},
            {block: 'main', content: [
                {elem: 'img', src: 'https://w-dog.ru/wallpapers/6/2/552010790246976/kotenok-myagkij-pushistyj-nezhnyj.jpg'},
                {tag: 'p', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat. Quisque ultrices velit vitae bibendum lobortis. Fusce posuere malesuada libero eget finibus. Mauris cursus lacinia auctor. Aliquam efficitur metus ac erat efficitur semper. Aenean aliquam nulla ac ex consectetur convallis. Maecenas mollis luctus sollicitudin. Integer tempor porttitor nibh, a semper ante tincidunt sed.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat. Quisque ultrices velit vitae bibendum lobortis. Fusce posuere malesuada libero eget finibus. Mauris cursus lacinia auctor. Aliquam efficitur metus ac erat efficitur semper. Aenean aliquam nulla ac ex consectetur convallis. Maecenas mollis luctus sollicitudin. Integer tempor porttitor nibh, a semper ante tincidunt sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat. Quisque ultrices velit vitae bibendum lobortis. Fusce posuere malesuada libero eget finibus. Mauris cursus lacinia auctor. Aliquam efficitur metus ac erat efficitur semper. Aenean aliquam nulla ac ex consectetur convallis. Maecenas mollis luctus sollicitudin. Integer tempor porttitor nibh, a semper ante tincidunt sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat. Quisque ultrices velit vitae bibendum lobortis. Fusce posuere malesuada libero eget finibus. Mauris cursus lacinia auctor. Aliquam efficitur metus ac erat efficitur semper. Aenean aliquam nulla ac ex consectetur convallis. Maecenas mollis luctus sollicitudin. Integer tempor porttitor nibh, a semper ante tincidunt sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dolor libero, dapibus in ante ac, tempus vulputate tortor. Duis gravida arcu quam. Sed lobortis interdum felis, eget rhoncus justo facilisis sed. Duis condimentum porttitor placerat. Quisque ultrices velit vitae bibendum lobortis. Fusce posuere malesuada libero eget finibus. Mauris cursus lacinia auctor. Aliquam efficitur metus ac erat efficitur semper. Aenean aliquam nulla ac ex consectetur convallis. Maecenas mollis luctus sollicitudin. Integer tempor porttitor nibh, a semper ante tincidunt sed. '},
              ],
            },
          ],
        },
        {block: 'layout-footer', content: [
            {tag: 'p', content: 'Footer text'},
          ],
        },
      ],
    },
  ],
};
