module.exports = {
  block: 'page',
  content: [
    {
      cls: 'container related-list', content: [
        {cls: 'row', content: [
            {cls: 'col-12 col-lg-4', content: [
                {block: 'linked-select', name: 'car-mark', id: 'car-mark', data_target: '#car-model', data_placeholder: 'Выберите марку', cls: 'form-control'},
              ],
            },
            {cls: 'col-12 col-lg-4', content: [
                {block: 'linked-select', name: 'models', id: 'car-model', data_target: '#car-year', data_placeholder: 'Выберите модель', cls: 'form-control'},
              ],
            },
            {cls: 'col-12 col-lg-4', content: [
                {block: 'linked-select', name: 'years', id: 'car-year', data_placeholder: 'Выберите год', cls: 'form-control'},
              ],
            },
          ],
        },
      ],
    },
  ],
};
/*
<div class="container related-list">
  <div class="row">
    <div class="col-12 col-lg-4">
      <select name="car-mark" id="car-mark" data-target="#car-model" class="form-control" data-placeholder="Выберите марку"></select>
    </div>
    <div class="col-12 col-lg-4">
      <select name="models" id="car-model" data-target="#car-year"  class="form-control" data-placeholder="Выберите модель"></select>
    </div>
    <div class="col-12 col-lg-4">
      <select name="years" id="car-year" class="form-control" data-placeholder="Выберите год"></select>
    </div>
  </div>
</div>
 */
