module.exports = {
  block: 'page',
  title: 'умный поиск',
  content: [
    {block: 'smart-filter', content: [
        {block: 'slider-wrapper', content: [
            {block: 'h', size: 5, content: 'Цена'},
            {block: 'slider'},
            {elem: 'wrap', content: [
                {block: 'h', size: 5, content: 'От'},
                {block: 'form-control', mix: {block: 'input-number'}, type: 'number'},
                {block: 'h', size: 5, content: 'До'},
                {block: 'form-control', mix: {block: 'input-number2'}, type: 'number'},
              ],
            },
          ],
        },
        {block: 'slider-wrapper', content: [
            {block: 'h', size: 5, content: 'Бренд'},
            {block: 'form-check', content: [
                {block: 'form-check-label', content: [
                    {block: 'form-check-input', value: 'one'},
                    'Option one',
                  ],
                },
                {block: 'form-check-label', content: [
                    {block: 'form-check-input', value: 'two'},
                    'Option one',
                  ],
                },
                {block: 'form-check-label', content: [
                    {block: 'form-check-input', value: 'three'},
                    'Option one',
                  ],
                },
              ],
            },
          ],
        },
        {block: 'slider-wrapper', content: [
            {block: 'filter', content:
                [
                  {block: 'h', size: 5, content: 'Вид'},
                  {block: 'select', cls: 'form-control', data_placeholder: 'Выберите вид', content:
                      [
                        {elem: 'option', value: '', content: ''},
                        {elem: 'option', value: 'type_1', content: 'Тип 1'},
                        {elem: 'option', value: 'type_2', content: 'Тип 2'},
                        {elem: 'option', value: 'type_3', content: 'Тип 3'},
                        {elem: 'option', value: 'type_4', content: 'Тип 4'},
                        {elem: 'option', value: 'type_5', content: 'Тип 5'},
                      ],
                  },
                ],
            },
          ],
        },
        {block: 'slider-wrapper', content: [{block: 'btn', tag: 'input', type: 'submit', content: 'Submit'},
          ],
        },
      ],
    },
  ],
};


/*
<button type="submit" class="btn btn-primary">Применить</button>
      <button type="reset" class="btn btn-secondary">Сбросить</button>
 */
