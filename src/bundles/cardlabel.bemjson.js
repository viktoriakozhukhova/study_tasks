module.exports = {
  block: 'page',
  content: [
    {cls: 'container', content: [
        {cls: 'row', content: [...new Array(9)].map((v, index) => (
            {cls: 'col-12 col-md-6 col-xl-4', content: [
                {block: 'labeled-card', cls: 'card', content: [
                    {block: 'sale', content: [
                      {elem: 'icon', content: 'sale'},
                      ],
                    },
                    {block: 'img', src: 'https://via.placeholder.com/348x150', cls: 'card-img'},
                    {cls: 'card-body', content: [
                        {block: 'h', size: 3, content: 'Название'},
                        {cls: 'description', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\n' +
                            '              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco\n' +
                            '              laboris nisi ut aliquip ex ea commodo consequat.'},
                      ],
                    },
                  ],
                },
              ],
            }
          ))},
      ],
    },
  ],
};
