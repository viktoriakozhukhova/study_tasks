module.exports = function(bh) {
  bh.match('form-check-label', function(ctx, json) {
    ctx.tag('label');
    ctx.attr('value', json.value);
  });
};
