module.exports = function(bh) {
  bh.match('article-shortened', function(ctx, json) {
    ctx.tag('article').attr('data-toggle', 'ellipsis');
  });
};
