module.exports = function(bh) {
  bh.match('filter__select', function(ctx, json) {
    ctx.tag('select');
    ctx.attr('name', 'type');
    ctx.attr('data-placeholder', json.data_placeholder);
  });
};
