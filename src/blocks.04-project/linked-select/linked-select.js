import $ from 'jquery';
import 'select2';
const CAR_MARKS = [
  'BMW',
  'Audi',
];
const CAR_DATA = {
  models: {
    BMW: ["X3", "X5", "X6"],
    Audi: ["Q3", "Q5", "Q7"]
  },
  years: {
    X3: ["2006", "2010", "2014"],
    X5: ["2006", "2010", "2013"],
    X6: ["2007", "2012", "2014"],
    Q3: ["2011", "2014", "2017"],
    Q5: ["2008", "2012", "2017"],
    Q7: ["2005", "2009", "2015"]
  }
};

class FormControl {
  constructor(elem) {
    this.block = elem;

    switch (this.block.tagName) {
      case "SELECT":
        this.initSelect();
        break;
      default:
    }
  }

  initSelect() {
    const $block = $(this.block);
    const defaultOptions = {
      width: "style",
      placeholder: this.block.getAttribute("data-placeholder") || false,
      containerCssClass: "form-control",
      dropdownCssClass: "form-control__dropdown",
      dropdownParent: this.block.dataset.parent
        ? $(this.block.dataset.parent)
        : $(document.body)
    };

    $block.select2(defaultOptions);

    $block.on("select2:select", () => {
      const event = document.createEvent("Event");
      event.initEvent("change", true, false);
      this.block.dispatchEvent(event);
    });
  }
}

class RelatedList {
  constructor(elem) {
    this.block = elem;
    this.controls = [...this.block.querySelectorAll("select")];

    this.changeHandler = this.changeHandler.bind(this);

    this.init();
  }

  init() {
    this.controls.forEach(control => this.toggleDisable(control));
    const carsMark = new Promise((resolve) => {
      setTimeout(() => {
        resolve(CAR_MARKS);
      }, 2000);
    });

    carsMark.then((marks) => {
      this.appendOptions(marks, this.controls[0]);
      this.block.addEventListener('change', this.changeHandler);
      this.controls.forEach(control => this.toggleDisable(control));
    });
  }

  appendOptions(array, target){
    // Пустое значение для вывода placeholder
    ['', ...array].forEach((item) => target.append(new Option(item, item)));
  }
  removeOptions(target){
    [...target.options].forEach((option) => option.remove());
  }
  removeOptionsRecursive(elem){
    this.removeOptions(elem);
    const target = this.block.querySelector(elem.dataset.target);
    if(target) this.removeOptionsRecursive(target);
  }
  toggleDisable(target){
    target.disabled = !target.disabled;
  }
  toggleDisableRecursive(elem){
    this.toggleDisable(elem);
    const target = this.block.querySelector(elem.dataset.target);
    if(target) this.toggleDisableRecursive(target);
  }

  async changeHandler(event) {
    const target = this.block.querySelector(event.target.dataset.target);
    if(!target) return;
    this.toggleDisableRecursive(target);
    this.removeOptionsRecursive(target);
    const data = await fetchData(target.name, event.target.value);
    this.appendOptions(data, target);
    this.toggleDisableRecursive(target);
  }
}

function fetchData (name, key) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(CAR_DATA[name][key]);
    }, 2000);
  });
}

[...document.getElementsByClassName("form-control")].forEach(
  (block) => new FormControl(block)
);

[...document.getElementsByClassName("related-list")].forEach(
  (block) => new RelatedList(block)
);
