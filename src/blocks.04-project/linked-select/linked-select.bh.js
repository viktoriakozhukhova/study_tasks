module.exports = function(bh) {
  bh.match('linked-select', function(ctx, json) {
    ctx.tag('select');
    ctx.attr('name', json.name);
    ctx.attr('id', json.id);
    ctx.attr('data-target', json.data_target);
    ctx.attr('data-placeholder', json.data_placeholder);
    ctx.attr('class', json.cls);
  });
};
