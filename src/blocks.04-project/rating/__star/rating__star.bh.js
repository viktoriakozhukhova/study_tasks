module.exports = function(bh) {
  bh.match('rating__star', function(ctx, json) {
    ctx.tag('input').attrs({
      'id': json.id,
      'name': 'rating',
      'value': json.value,
    });
    ctx.attr('type', 'radio');
  });
};
