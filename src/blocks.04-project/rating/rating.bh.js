module.exports = function(bh) {
  bh.match('rating', function(ctx, json) {
    ctx.tag('div').attrs({
      'data-product-id': ctx.generateId(),
    }).content([...new Array(10)].map((v, index) => [
      {elem: 'star', id: 'rating-' + index+1, name: 'rating', value: 10-index},
      {elem: 'label', for: 'rating-' + index+1, title: (5-index/2)+' из 5', cls: index % 2 ? 'half' : ''},
    ]));
  });
};
