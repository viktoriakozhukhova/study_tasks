module.exports = {
    mustDeps: [],
    shouldDeps: [
        {block: 'font-awesome'},
        {elem: 'star'},
        {elem: 'label'},
    ],
};
