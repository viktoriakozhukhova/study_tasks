module.exports = function(bh) {
  bh.match('rating__label', function(ctx, json) {
    ctx.tag('label').attrs({
      'for': json.for,
      'title': json.title,
    });
    ctx.attr('type', 'radio');
  });
};
