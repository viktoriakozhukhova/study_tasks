// eslint-disable-next-line require-jsdoc
function Rating(elem) {
  const productId = elem.dataset.productId;
  const inputs = Array.from(elem.querySelectorAll('input'));

  elem.addEventListener('change', ({target}) => {
    // Задизейблить рейтинг
    inputs.forEach((input) => input.disabled = true);
    elem.classList.add('disabled');

    const data = {
      productId,
      value: target.value,
    };

    // Заглушка запроса
    fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify(data),
    }).then(() => {
      const randomRating = Math.floor(Math.random() * 9) + 1; // От 1 до 10
      inputs[randomRating].checked = true;
    });
  });
}

const ratingColl = Array.from(document.getElementsByClassName('rating'));
ratingColl.forEach((ratingEl) => new Rating(ratingEl));
