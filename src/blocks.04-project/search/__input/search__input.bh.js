module.exports = function(bh) {
  bh.match('search__input', function(ctx, json) {
    ctx.tag('input').attrs({
      'placeholder': json.placeholder,
    });
    ctx.attr('type', 'search');
  });
};
