module.exports = function(bh) {
  bh.match('select__option', function(ctx, json) {
    ctx.tag('option');
    ctx.attr('value', json.value);
  });
};
