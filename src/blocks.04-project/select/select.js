import $ from 'jquery';
import 'select2';
class FormControl {
  constructor(elem) {
    this.block = elem;

    switch (this.block.tagName) {
      case 'SELECT':
        this.initSelect();
        break;
      default:
    }
  }

  initSelect() {
    const $block = $(this.block);
    const defaultOptions = {
      width: 'style',
      placeholder: this.block.getAttribute('data-placeholder') || false,
      containerCssClass: 'form-control',
      dropdownCssClass: 'form-control__dropdown',
      dropdownParent: this.block.dataset.parent ? $(this.block.dataset.parent) : $(document.body),
    };

    $block.select2(defaultOptions);
  }
}


[...document.getElementsByClassName('form-control')].forEach((block) => new FormControl(block));
