module.exports = function(bh) {
  bh.match('form-check-input', function(ctx, json) {
    ctx.tag('input');
    ctx.attr('type', 'checkbox');
  });
};
