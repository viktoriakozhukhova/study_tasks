import * as noUiSlider from 'nouislider';
import 'nouislider/dist/nouislider.css';
var slider = document.querySelector('.slider');

noUiSlider.create(slider, {
  start: [20, 80],
  connect: true,
  range: {
    'min': 0,
    'max': 100,
  },
});


var inputNumber = document.querySelector('.input-number');
var inputNumber2 = document.querySelector('.input-number2');

slider.noUiSlider.on('update', function(values, handle) {
  var value = values[handle];

  if (handle) {
    inputNumber2.value = Math.floor(value);
  } else {
    inputNumber.value = Math.floor(value);
  }
});

inputNumber.addEventListener('change', function() {
  slider.noUiSlider.set([this.value]);
});

inputNumber2.addEventListener('change', function() {
  slider.noUiSlider.set([this.value]);
});
